#!/usr/bin/env python3
'Prompt the user to select an input device using FZF.'

'''
Lots of room for improvement here, but it gets the job done:

- could display capabilities (as in libinput list-devices)
- could sort by capabilities
- could filter by capabilities
- could auto-select (eg, first / all / only devices to have capabilities)

Note that it requires pyfzf and evdev
'''

import sys
import evdev
from pyfzf.pyfzf import FzfPrompt
fzf = FzfPrompt()

def make_string(path):
    dev = evdev.InputDevice(path)
    return f"{dev.name} ({path})"


def prompt(fzf_args = ['--multi']):
    '''Prompt the user to select an input device using FZF'''

    # index paths by display string, since that's what fzf returns
    devices = {make_string(path): path for path in evdev.list_devices()}
    selected = fzf.prompt(devices.keys(), ' '.join(fzf_args))

    # recall paths using display string as key
    return [devices[key] for key in selected]


def main():
    '''Performs the prompt, passes all arguments directly to fzf'''
    selected_paths = prompt(sys.argv[1:])
    print(' '.join(selected_paths))


if __name__ == '__main__':
    main()
