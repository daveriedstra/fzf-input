# fzf-input

Select an input device using FZF.

Requires [pyfzf](https://github.com/nk412/pyfzf) and [python-evdev](https://github.com/gvalkov/python-evdev). Passes all arguments to fzf, `--multi` is enabled by default.
